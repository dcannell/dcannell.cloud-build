osp-instances
=============

Create DTQ instances for 3-tier application on OSP.

Requirements
------------

 - Supporting OSP infrastructure is available.

Role Variables
--------------

 - Per server configuration is kept under vars/

Dependencies
------------

 - Current requires OSP cloud named 'ospcloud'

License
-------

BSD

Author Information
------------------

Dennis Cannell
