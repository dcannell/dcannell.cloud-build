appdb
=====

Perform configuration of a database server.  Initialise database, if needed.

Requirements
------------

 - Supporting infrastructure is available.  
 - Instances are up, and available, in either AWS or OSP.
 - Base configuration has been executed

Role Variables
--------------

 - Packages to install and services to start are defined under vars/

Dependencies
------------

 -

License
-------

BSD

Author Information
------------------

Dennis Cannell
