aws-instances
=============

Setup environment appropriately to make an OPENTLC CFME API request

Requirements
------------

 - 

Role Variables
--------------

 - 

Dependencies
------------

 - Requires CFME authentication details to be stored in credential.rc

License
-------

BSD

Author Information
------------------

Dennis Cannell
