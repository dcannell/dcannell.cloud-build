base
====

Perform base configuration, common to all 3-tier application server instances

Requirements
------------

 - Supporting infrastructure is available.  
 - Instances are up, and available, in either AWS or OSP.

Role Variables
--------------

 - 

Dependencies
------------

 -

License
-------

BSD

Author Information
------------------

Dennis Cannell
