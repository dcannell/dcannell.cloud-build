osp-delete
==========

Delete an OSP server instance from a cloud

Requirements
------------

 - Supporting infrastructure is available.  
 - Instance is up, and available in OSP.

Role Variables
--------------

 - 

Dependencies
------------

 -

License
-------

BSD

Author Information
------------------

Dennis Cannell
