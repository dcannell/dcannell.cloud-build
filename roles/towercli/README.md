towercli
========

Install required components for Tower CLI access

Requirements
------------

 - Software repositories set up appropriately.

Role Variables
--------------

 - RPM & PIP packages defined in vars/ 

Dependencies
------------

 - 

License
-------

BSD

Author Information
------------------

Dennis Cannell
