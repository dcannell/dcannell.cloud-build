aws-facts
=========

Build in-memory inventory based upon instances and instance meta information
from AWS

Requirements
------------

 - Supporting AWS infrastructure is available.  
 - Instances are up, and available, in EC2.

Role Variables
--------------

 - 

Dependencies
------------

 -

License
-------

BSD

Author Information
------------------

Dennis Cannell
