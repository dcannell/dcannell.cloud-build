osp-facts
=========

Build in-memory inventory based upon instances and instance meta information
from OSP

Requirements
------------

 - Supporting OSP infrastructure is available.  
 - Instances are up, and available, in OSP.

Role Variables
--------------

 - 

Dependencies
------------

 -

License
-------

BSD

Author Information
------------------

Dennis Cannell
