osp-keypair
===========

Generate and insert OSP keypair

Requirements
------------

 - Supporting OSP infrastructure is available, with cloud 'ospcloud'

Role Variables
--------------

 - 

Dependencies
------------

 -

License
-------

BSD

Author Information
------------------

Prakhar Srivastava
