Homework Assignment
===================

Build, deploy and test QA 3-tier application stack in OSP.  If successful, continue on to build, deploy and test the same stack, as PROD, in AWS.  If unsuccessful, remove QA environment. 

Requirements
------------

 - OSP environment available.  As provisioned by: OTLC-LAB Ansible_Advance_OSP_ENV
 - OPEN TLC API access available to provision: OTLC-LAB PROD_THREE_TIER_APP
 - Vault password, if executing key insertion
 - OPEN TLC authentication details, if building PROD


Playbooks
---------

This project provides the following playbooks:

 - Provision_OSP_infra.yml - build OSP networks and securitygroups for QA
 - Provision_OSP_instances.yml - build instances in OSP for QA
 - Deploy_3tier_app.yml - configure and deploy 3-tier app to QA
 - Test_3tier_app.yml - test 3-tier app in QA
 - Provision_AWS_instances.yml - request build of infrastructure and instances for PROD
 - Test_AWS_instances.yml - confirm availability of instances
 - Insert_AWS_Key.yml - grab SSH key for Ansible access and insert into Tower
 - Deploy_AWS_3tier_app.yml - configure and deploy 3-tier app to PROD
 - Test_AWS_3tier_app.yml - test 3-tier app in QA
 - Delete_OSP_instances.yml - remove QA instances

Roles utilised by these playbooks are documented separately.

Improvements
------------

Possible improvements:

 - test from Jumpbox in QA, rather than from frontends
 - paramitise aws-instances further
 - start OSP instances if already created but shutdown
 - implement further tags
 - replace all occurences of with_items with loop
 - move more vars into default/main
 - move all vars up tree to main project dir
 - use vault for OTLC creds

License
-------

BSD

Author Information
------------------

Dennis Cannell
